/// Ngoài khác nhau về cú pháp vòng lặp for và forEach thì còn khác nhau như thế nào?

// Trả lời:
// Khác nhau về cách thực hiện:
// - Vòng lặp for: thực hiện theo thứ tự từng bước, từng lần lặp
// - forEach: thực hiện đồng thời, không đợi lần lượt như vòng lặp for

for (let i = 0; i < 10; i++) {
    console.log(i);
}

[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].forEach((item) => {
    console.log(item);
});

// Khác nhau về cách trả về kết quả:
// - Vòng lặp for: trả về kết quả cuối cùng
let result = 0;
for (let i = 0; i < 10; i++) {
    result += i;
}

// Khác nhau về cách sử dụng:

// - Vòng lặp for: sử dụng khi cần thực hiện theo thứ tự từng bước, từng lần lặp

// - forEach: sử dụng khi cần thực hiện đồng thời, không đợi lần lượt như vòng lặp for

// Khác nhau về cách truyền tham số:

// - Vòng lặp for: truyền tham số bằng cách gán giá trị cho biến

for (let i = 0; i < 10; i++) {
    console.log(i);
}

// - forEach: truyền tham số bằng cách truyền vào callback

[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].forEach((item, index) => {
    //item: giá trị của phần tử
    //index: vị trí của phần tử
    console.log(item);
});

// Khác nhau về cách dừng vòng lặp:
// Ở vòng lặp for, ta có thể dừng vòng lặp bằng cách sử dụng câu lệnh break với điều kiện ở đây là biến i === 5

for (let i = 0; i < 10; i++) {
    if (i === 5) {
        break;
    }
    console.log(i);
}

// - forEach: dừng vòng lặp bằng cách sử dụng return

[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].forEach((item, index) => {
    if (item === 5) {
        return;
    }
    console.log(item);
});

// Khác nhau về cách trả về giá trị:

// - Vòng lặp for: trả về giá trị bằng cách sử dụng return

function sum() {
    let result = 0;
    for (let i = 0; i < 10; i++) {
        result += i;
    }
    return result;
}

// - forEach: trả về giá trị bằng cách sử dụng return

function sum() {
    let result = 0;
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].forEach((item) => {
        result += item;
    });
    return result;
}
