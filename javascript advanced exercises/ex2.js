// Xử lý 20 eventAsynchronous sao cho thời gian chạy của chúng nhanh nhất
//Mỗi lần xử lý 5 eventAsynchronous
// // //Cách 1: dùng Promise.all
//Ý tưởng: tạo ra 4 promise, mỗi promise xử lý 5 eventAsynchronous
//Sau đó dùng Promise.all để xử lý 4 promise đó

// Tạo một function để khởi tạo promise có thời gian chạy truyền vào
const eventPromise = async (time) => {
    try {
        const result = await new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve('This is event ' + time);
            }, time);
        });
        return result;
    } catch (error) {
        console.log(error);
    }
};
////Tạo ra 4 mản chứa 5 promise mỗi mảng
const arrPromise1 = [];
const arrPromise2 = [];
const arrPromise3 = [];
const arrPromise4 = [];
for (let i = 0; i < 5; i++) {
    arrPromise1.push(eventPromise(Math.floor(Math.random() * 4000) + 1000));
    arrPromise2.push(eventPromise(Math.floor(Math.random() * 4000) + 1000));
    arrPromise3.push(eventPromise(Math.floor(Math.random() * 4000) + 1000));
    arrPromise4.push(eventPromise(Math.floor(Math.random() * 4000) + 1000));
}
// //Xử lý 4 promise đó bằng Promise.all nếu có
Promise.all(arrPromise1).then((result) => {
    console.log(result);
});

Promise.all(arrPromise2).then((result) => {
    console.log(result);
});

Promise.all(arrPromise3).then((result) => {
    console.log(result);
});

Promise.all(arrPromise4).then((result) => {
    console.log(result);
});
