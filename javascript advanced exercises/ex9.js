// Promise.all chỉ trả về kết quả khi tất cả các promise đều thực hiện thành công,
// chỉ cần 1 sự kiện thất bại thì sẽ trả về kết quả thất bại
// Và các sự kiện khác thành công nhưng không nhận được kết quả mong muốn

// Có 3 sự kiện không đồng bộ không biết trước thời gian thực hiện
// Giả sử 2 sự kiện thành công và 1 sự kiện thất bại
// Thực hiện 3 sự kiện đồng thời làm thế nào để trả về 2 kết quả thành công và 1 kết quả thất bại ['Thành công', 'Thành công', 'Thất bại']

const success1 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve('success1');
    }, 1000);
});

const success2 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve('success2');
    }, 1000);
});

const fail = new Promise((resolve, reject) => {
    setTimeout(() => {
        reject('fail');
    }, 1000);
});

const promisesResult = Promise.allSettled([success1, success2, fail]);

promisesResult
    .then((result) => {
        console.log(result);
    })
    .catch((error) => {
        console.log(error);
    });
