//Phân biệt setImmediate và process.nextTick
//SetImmediate: là một hàm được gọi sau khi hàm setTimeout với thời gian delay là 0
//Process.nextTick: là một hàm được gọi ngay sau khi hàm hiện tại thực thi xong

console.log('Start');
setTimeout(() => {
    console.log('setTimeout');
});

// // //Process.nextTick
process.nextTick(() => {
    console.log('Process.nextTick');
});

setImmediate(() => {
    console.log('SetImmediate');
});

// Sau khi thực thi lệnh console.log('Start'); thì process.nextTick sẽ được gọi ngay sau đó
// Sau khi thực thi lệnh process.nextTick thì setTimeout sẽ được gọi với thời gian delay là 0
// Sau khi thực thi lệnh setTimeout thì setImmediate sẽ được gọi
