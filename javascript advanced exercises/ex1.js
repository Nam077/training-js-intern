const run = (milliseconds) => {
    const startTime = new Date().getTime();
    while (new Date().getTime() < startTime + milliseconds) {
        // do nothing
    }
};

run(5000);
