const controller = async (req, res) => {
    const arr = Array.from(Array(1000000).keys());
    arr.forEach((item) => {
        console.log(item);
    });
    res.status(200).end();
};
//Giả sử:
//Array có 1 triệu phần tử, cần lặp qua từng phần tử dể thực hiện một công việc nào đó
//Cách thực hiện thông thường là sử dụng vòng lặp for hoặc forEach
// Có 3 request gửi đến server
//Viết một function lặp để thay thế forEach để các request thực hiện đồng thời, không đợi lân lượt như forEach
const newLoop = (arr, callback) => {
    //khởi tạo một mảng chứa các promise
    const promises = [];
    //lặp qua từng phần tử của mảng
    arr.forEach((item) => {
        //tạo ra một promise
        const promise = new Promise((resolve) => {
            //thực hiện callback và truyền vào item
            callback(item);
            //sau khi thực hiện xong thì gọi resolve
            resolve();
        });
        //thêm promise vào mảng
        promises.push(promise);
    });
    //trả về một promise all
    return Promise.all(promises);
};
