//Vấn đề của event loop
//vấn đề của event loop là khi chạy một hàm setTimeout thì hàm này sẽ được đẩy vào hàng đợi của event loop
//sau đó nó sẽ chạy các hàm khác trong hàng đợi event loop
//sau khi chạy xong các hàm trong hàng đợi event loop thì nó sẽ chạy hàm setTimeout

const sleep = (ms) => {
    const start = new Date().getTime();
    while (new Date().getTime() < start + ms) {}
};
function doA() {
    sleep(10000);
}
async function doB() {
    setTimeout(() => {
        console.log('doB');
    }, 1000);
}
const controller = async (req, res) => {
    doA();
    await doB();
};

const controller = async (req, res) => {
    new Promise((resolve) => {
        doA();
        resolve();
    });
    await doB();
};
//Giả sử có 3 request gửi đến server cùng lúc
//Thì request đầu tiên sẽ nhận được response sau 30s
// Cách cải thiện để tất cả các request nhận được response sau 11s
//Cách 1: sử dụng async await

//Cách 2: sử dụng promise
const controller = async (req, res) => {
    new Promise((resolve) => {
        doA();
        resolve();
    });
    await doB();
};
