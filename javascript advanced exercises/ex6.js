// Liệt kê các phương pháp clone object trong javascript
// Cách 1: Sử dụng Object.assign()
const obj1 = {
    name: 'Nguyen Van A',
    age: 20,
};
const obj2 = Object.assign({}, obj1);
// Giải thích: Object.assign() sẽ tạo ra một object mới và copy các thuộc tính của object cũ vào object mới

// Cách 2: Sử dụng spread operator
const obj3 = {
    ...obj1,
};
// Giải thích: spread operator sẽ tạo ra một object mới và copy các thuộc tính của object cũ vào object mới

// Cách 3: Sử dụng JSON.parse(JSON.stringify())
const obj4 = JSON.parse(JSON.stringify(obj1));
// Giải thích: JSON.parse() sẽ chuyển đổi một chuỗi JSON thành một object và JSON.stringify() sẽ chuyển đổi một object thành một chuỗi JSON
// Cách 4: Sử dụng for in
const obj5 = {};
for (const key in obj1) {
    obj5[key] = obj1[key];
}
// Giải thích: for in sẽ duyệt qua các thuộc tính của object và copy các thuộc tính của object cũ vào object mới
// Cách 5: Sử dụng Object.keys()
const obj6 = {};
Object.keys(obj1).forEach((key) => {
    obj6[key] = obj1[key];
});
// Giải thích: Object.keys() sẽ trả về một mảng các key của object và forEach() sẽ duyệt qua các phần tử của mảng và copy các thuộc tính của object cũ vào object mới

// Cách 6: Sử dụng Object.entries()
const obj7 = {};
Object.entries(obj1).forEach(([key, value]) => {
    obj7[key] = value;
});

// Giải thích: Object.entries() sẽ trả về một mảng các key và value của object và forEach() sẽ duyệt qua các phần tử của mảng và copy các thuộc tính của object cũ vào object mới

// Cách 7: Sử dụng Object.values()
const obj8 = {};
Object.values(obj1).forEach((value) => {
    obj8[value] = value;
});
// Giải thích: Object.values() sẽ trả về một mảng các value của object và forEach() sẽ duyệt qua các phần tử của mảng và copy các thuộc tính của object cũ vào object mới
