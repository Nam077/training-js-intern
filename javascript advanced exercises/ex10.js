//await hay return await?

async function waitAndMaybeReject() {
    new Promise((r) => setTimeout(r, 1000));
    throw new Error('This is an error');
}

async function test1() {
    try {
        await waitAndMaybeReject();
    } catch (error) {
        console.log('Oh No!');
    }
}

async function test2() {
    try {
        return await waitAndMaybeReject();
    } catch (error) {
        throw error;
    }
}
async function test3() {
    return await waitAndMaybeReject();
}

function test4() {
    return waitAndMaybeReject();
}

const main = async () => {
    const value1 = await test1();
    console.log(value1);
};
main().then(() => console.log('done'));
