// phân biệt let và const trường hợp nào nên dùng let, trường hợp nào nên dùng const
// let và const là 2 từ khóa khai báo biến trong javascript
// let dùng để khai báo biến có thể thay đổi giá trị
// const dùng để khai báo biến không thể thay đổi giá trị

// ví dụ về let
let a = 1;
a = 2;
console.log(a); // 2

// ví dụ về const
const b = 1;
// b = 2; // lỗi
console.log(b);
// Trường hợp object được khai báo bằng const thì các giá trị của object có thể thay đổi
const c = {
    name: 'John',
    age: 20,
};
c.name = 'Peter';
console.log(c); // { name: 'Peter', age: 20 }
//Vì vậy nên dùng const để khai báo object, dùng let để khai báo các biến có thể thay đổi giá trị
