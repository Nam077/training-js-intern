// Phân biệt callback, promise, async/await
// Callback: là một hàm được truyền vào một hàm khác như một tham số và được gọi lại bên trong hàm đó.
//
// Promise: là một đối tượng được sử dụng để thực thi một hàm bất đồng bộ và trả về một kết quả sau khi hoàn thành.

// Async/await: là một cú pháp để viết code bất đồng bộ một cách đơn giản hơn và dễ hiểu hơn.

// //Callback

// //Khai báo hàm callback
function callback() {
    console.log('This is callback function');
}

function callback2() {
    console.log('This is callback2 function');
}

function callbackFunction(callback) {
    callback();
}

// //Gọi hàm callback
callbackFunction(callback);
callbackFunction(callback2);


// //Promise
// //Khai báo promise
const promise = new Promise((resolve, reject) => {
        //resolve: trả về kết quả thành công
        resolve('This is resolve');
        //reject: trả về kết quả thất bại
        reject('This is reject');
    }
);
// //Gọi promise và xử lý kết quả nếu thành công hoặc thất bại bằng then và catch
// //then: xử lý kết quả thành công
// //catch: xử lý kết quả thất bại
promise.then((result) => {
    console.log(result);
}).catch((error) => {
    console.log(error);
});

// //Async/await
// //Khai báo async function
async function asyncFunction() {
    return 'This is async function';
}

// //Gọi async function
asyncFunction2().then((result) => {
    console.log(result);
});


//sử dụng async/await
async function asyncFunction2() {
    const result = await asyncFunction();
    return result;
}

// //Gọi async function
asyncFunction2().then((result) => {
        console.log(result);
    }
);