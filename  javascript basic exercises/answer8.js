//Phân biệt địa chỉ và giá trị cuả biến
// //Địa chỉ: là vị trí của biến trong bộ nhớ
// //Giá trị: là giá trị của biến
//
// //Ví dụ
let a = 1;
let b = a;
console.log(a, b); // 1 1
a = 2;
console.log(a, b); // 2 1
// //Khi gán biến b bằng biến a thì địa chỉ của biến b sẽ trỏ đến địa chỉ của biến a
// //Khi thay đổi giá trị của biến a thì giá trị của biến b không thay đổi
// //Khi thay đổi giá trị của biến b thì giá trị của biến a không thay đổi
// //Vì vậy nên dùng const để khai báo object, dùng let để khai báo các biến có thể thay đổi giá trị
