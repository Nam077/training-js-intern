//arrow function là gì? so sánh với function expression
// //Arrow function là một cách viết ngắn gọn hơn của function expression
// // //Ví dụ
function sum(a, b) {
    return a + b;
}
// // //Arrow function
const sum2 = (a, b) => {
    return a + b;
};
console.log(sum(1, 2), sum2(1, 2)); // 3 3
