// phân biệt giữa forEarch, map, filter, reduce,và for trong javascript
// // forEarch: dùng để duyệt qua các phần tử của mảng
// // map: dùng để duyệt qua các phần tử của mảng và trả về một mảng mới
// // filter: dùng để duyệt qua các phần tử của mảng và trả về một mảng mới với các phần tử thỏa mãn điều kiện
// // reduce: dùng để duyệt qua các phần tử của mảng và trả về một giá trị
// // for: dùng để duyệt qua các phần tử của mảng
//
// // ví dụ về forEarch
const array = [1, 2, 3, 4, 5];
array.forEach((item) => {
    console.log(item);
});

// // ví dụ về map trả về một mảng mới có các phần tử là nhân đôi của mảng cũ
const array2 = array.map(function (item) {
    return item * 2;
});
console.log(array2);

// // ví dụ về filter lọc ra các số lơn hơn 3
const array3 = array.filter(function (item) {
    return item > 3;
});
console.log(array3);
// // ví dụ về reduce tính tổng các phần tử của mảng array tổng khởi tạo là 0
const array4 = array.reduce(
    // callback function có 2 tham số là total và item
    (total, item) => {
        return total + item;
    },
    // khởi tạo tổng là 0
    0,
);
console.log(array4);

// // ví dụ về for
for (let i = 0; i < array.length; i++) {
    console.log(array[i]);
}
