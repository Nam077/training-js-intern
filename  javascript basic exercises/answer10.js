// Làm thế nào để kiểm tra một Object rông hay không?
// // // Cách 1: dùng Object.keys(object).length
const object = {};
//keys là một mảng các key của object
if (Object.keys(object).length === 0) {
    console.log('object rỗng');
}

// Cách 2: dùng Object.entries(object).length
//entries là một mảng các mảng có 2 phần tử là key và value của object
if (Object.entries(object).length === 0) {
    console.log('object rỗng');
}

// Cách 3: dùng Object.getOwnPropertyNames(object).length
//getOwnPropertyNames là một mảng các key của object
if (Object.getOwnPropertyNames(object).length === 0) {
    console.log('object rỗng');
}
// // // Cách 4: dùng Object.values(object).length
//values là một mảng các value của object
if (Object.values(object).length === 0) {
    console.log('object rỗng');
}
