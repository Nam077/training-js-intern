//spread operator là gì?
// // //Spread operator là một cách viết ngắn gọn hơn của function apply
// // // //Ví dụ
const arr = [1, 2, 3];
const arr2 = [4, 5, 6];
const arr3 = [...arr, ...arr2];
console.log(arr3); // 6
