// Promise hell là gì?
// // //Promise hell là một kỹ thuật xử lý bất đồng bộ bằng cách lồng nhau nhiều hàm promise

// // //Ví dụ về promise hell 1
function promiseHell() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('Log after 1s');
            resolve();
        }, 1000);
    })
        .then(() => {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    console.log('Log after 2s');
                    resolve();
                }, 1000);
            });
        })
        .then(() => {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    console.log('Log after 3s');
                    resolve();
                }, 1000);
            });
        })
        .then(() => {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    console.log('Log after 4s');
                    resolve();
                }, 1000);
            });
        });
}

// //Gọi hàm promise hell
promiseHell().then();
