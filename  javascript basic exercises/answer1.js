//Phân biệt setTimeout và setInterval
//setTimeout: chỉ chạy 1 lần
//khai báo: setTimeout(function, time)
//khi time hết thì function sẽ được thực thi

//setInterval: chạy liên tục
//khai báo: setInterval(function, time)
//Sau mỗi khoảng time thì function sẽ được thực thi lại


setTimeout(() => {
    console.log('Log after 1s');
}, 1000);

//setInterval: chaỵ liên tục sau 1 khoảng thời gian nhất định ở đây là 1s (1000ms)
let index = 1;
const setInterValTest = setInterval(() => {
    console.log('Log time: ', index);
    index++;
}, 1000);

//clearInterval: dừng setInterval khi nào cần thiết
//o đây là sau 5s

setTimeout(() => {
    clearInterval(setInterValTest);
    console.log('Stop setInterval');
}, 5000);

//setTimeout dừng chạy khi nào cần thiết
//o đây là sau 3s
setTimeout(() => {
    console.log('Stop setTimeout');
}, 3000);