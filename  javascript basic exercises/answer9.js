//Các kiểu dữ liệu trong javascript
//JavaScript có tất cả 7 kiểu dữ liệu nguyên thủy:
//Boolean (true/false)
//Null (null)
//Undefined (undefined)
//Number (bao gồm cả số nguyên và số thực)
//BigInt (số nguyên lớn)
//String (chuỗi)
//Symbol (new in ECMAScript 2015) (symbol)
//--Các kiểu dữ liệu phức tạp:
//Object (bao gồm cả Array, Function, Date, RegExp, ...)
//Function (hàm)
//Array (mảng)
//Date (ngày tháng)
//RegExp (biểu thức chính quy)
//
