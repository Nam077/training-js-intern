//Các phương pháp nối 2 mảng trong javascript
// phương pháp 1: dùng concat
const arr1 = [1, 2, 3];
const arr2 = [4, 5, 6];
const arr3 = arr1.concat(arr2);
console.log(arr3);

// phương pháp 2: dùng spread operator
const arr4 = [...arr1, ...arr2];
console.log(arr4);
// phương pháp 3: dùng push
const arr5 = arr1.push(...arr2);
