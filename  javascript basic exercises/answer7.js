//Các phương pháp clone object trong javascript
// // // phương pháp 1: dùng Object.assign
const obj1 = {
    name: 'John',
    age: 20,
};

const obj2 = Object.assign({}, obj1);
console.log(obj2);

// // // phương pháp 2: dùng spread operator
const obj3 = {
    ...obj1,
};
console.log(obj3);

// // // phương pháp 3: dùng JSON.parse và JSON.stringify
const obj4 = JSON.parse(JSON.stringify(obj1));
console.log(obj4);

//dùng thư viện lodash
