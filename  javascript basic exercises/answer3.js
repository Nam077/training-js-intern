//Callback hell là gì?
// //Callback hell là một kỹ thuật xử lý bất đồng bộ bằng cách lồng nhau nhiều hàm callback

// //Ví dụ về callback hell 1
function callbackHell() {
    setTimeout(() => {
        console.log('Log after 1s');
        setTimeout(() => {
            console.log('Log after 2s');
            setTimeout(() => {
                console.log('Log after 3s');
                setTimeout(() => {
                    console.log('Log after 4s');
                }, 1000);
            }, 1000);
        }, 1000);
    }, 1000);
}

// //Gọi hàm callback hell
callbackHell();

