//con trỏ this là gì? phân biệt call, apply, bind

//this là một con trỏ trỏ đến đối tượng hiện tại
// //Ví dụ
const person = {
    name: 'John',
    age: 20,
    getName: function () {
        return this.name;
    },
    setName: function (name) {
        this.name = name;
    },
};
person.setName('Peter');
console.log(person.getName()); // John
// // //Khi gọi phương thức getName() thì this sẽ trỏ đến đối tượng person

// // // //call, apply, bind là các phương thức của function
//Ví dụ
function sum(a, b) {
    return a + b;
}
//call
console.log(sum.call(null, 1, 2)); // 3
//apply
console.log(sum.apply(null, [1, 2])); // 3
//bind
const sum2 = sum.bind(null, 1, 2);
console.log(sum2()); // 3
// // // //call, apply, bind đều dùng để gọi một hàm
// // // //call và apply đều truyền vào tham số là một đối tượng và các tham số của hàm
// // // //bind truyền vào tham số là một đối tượng và các tham số của hàm, trả về một hàm mới
// // // //Khi gọi hàm mới trả về bằng bind thì this sẽ trỏ đến đối tượng được truyền vào
// // // //Ví dụ
const person2 = {
    name: 'John',
    age: 20,
};
function sum3(a, b) {
    return this.name + a + b;
}
const sum4 = sum3.bind(person2, 1, 2);
console.log(sum4()); // John12
